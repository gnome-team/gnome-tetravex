# Canadian English translation for gnome-games package.
# Copyright (C) 2012 gnome-games COPYRIGHT HOLDER
# This file is distributed under the same licence as the gnome-games package.
# Adam Weinberger <adamw@gnome.org>, 2004, 2005, 2006.
# Tiffany Antopolski <tiffany.antopolski@gmail.com>, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-games\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-02-04 04:50+0100\n"
"PO-Revision-Date: 2012-02-02 18:40-0500\n"
"Last-Translator: Tiffany Antopolski <tiffany.antopolski@gmail.com>\n"
"Language-Team: Canadian English\n"
"Language: en_CA\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../data/gnome-tetravex.appdata.xml.in.h:1
#, fuzzy
msgid "GNOME Tetravex"
msgstr "Tetravex"

#: ../data/gnome-tetravex.appdata.xml.in.h:2
msgid "Reorder tiles to fit a square"
msgstr ""

#: ../data/gnome-tetravex.appdata.xml.in.h:3
msgid ""
"Each square piece has a number on each side. Position the pieces on the left "
"side of the board so that the same numbers are next to each other. If the "
"numbers don't match, you can't put the piece there. Move quickly: you're "
"being timed!"
msgstr ""

#: ../data/gnome-tetravex.appdata.xml.in.h:4
msgid ""
"You can make the game easier or harder by changing the size of the board."
msgstr ""

#: ../data/gnome-tetravex.desktop.in.h:1 ../src/gnome-tetravex.ui.h:1
#: ../src/gnome-tetravex.vala:71 ../src/gnome-tetravex.vala:106
#: ../src/gnome-tetravex.vala:381
msgid "Tetravex"
msgstr "Tetravex"

#: ../data/gnome-tetravex.desktop.in.h:2
msgid "Complete the puzzle by matching numbered tiles"
msgstr "Complete the puzzle by matching numbered tiles"

#: ../data/gnome-tetravex.desktop.in.h:3
msgid "game;logic;board;"
msgstr ""

#: ../data/org.gnome.tetravex.gschema.xml.h:1
msgid "The size of the playing grid"
msgstr "The size of the playing grid"

#: ../data/org.gnome.tetravex.gschema.xml.h:2
msgid "The value of this key is used to decide the size of the playing grid."
msgstr "The value of this key is used to decide the size of the playing grid."

#: ../data/org.gnome.tetravex.gschema.xml.h:3
#, fuzzy
msgid "Width of the window in pixels"
msgstr "The width of the main window in pixels."

#: ../data/org.gnome.tetravex.gschema.xml.h:4
#, fuzzy
msgid "Height of the window in pixels"
msgstr "The height of the main window in pixels."

#: ../data/org.gnome.tetravex.gschema.xml.h:5
msgid "true if the window is maximized"
msgstr ""

#: ../src/app-menu.ui.h:1
msgid "_New Game"
msgstr "_New Game"

#: ../src/app-menu.ui.h:2
msgid "_Scores"
msgstr "_Scores"

#: ../src/app-menu.ui.h:3
#, fuzzy
msgid "_Size"
msgstr "Size:"

#: ../src/app-menu.ui.h:4
msgid "_2x2"
msgstr ""

#: ../src/app-menu.ui.h:5
msgid "_3x3"
msgstr ""

#: ../src/app-menu.ui.h:6
msgid "_4x4"
msgstr ""

#: ../src/app-menu.ui.h:7
msgid "_5x5"
msgstr ""

#: ../src/app-menu.ui.h:8
msgid "_6x6"
msgstr ""

#: ../src/app-menu.ui.h:9
msgid "_Help"
msgstr "_Help"

#: ../src/app-menu.ui.h:10
msgid "_About"
msgstr ""

#: ../src/app-menu.ui.h:11
msgid "_Quit"
msgstr ""

#: ../src/gnome-tetravex.vala:38
#, fuzzy
msgid "Print release version and exit"
msgstr "Show release version"

#: ../src/gnome-tetravex.vala:39
#, fuzzy
msgid "Start the game paused"
msgstr "Restart the game"

#: ../src/gnome-tetravex.vala:40
#, fuzzy
msgid "Set size of board (2-6)"
msgstr "The size of the game board."

#. not a typo
#: ../src/gnome-tetravex.vala:129
#, fuzzy
msgid "Resume the game"
msgstr "Resume the paused game"

#: ../src/gnome-tetravex.vala:141
msgid "Pause the game"
msgstr "Pause the game"

#: ../src/gnome-tetravex.vala:158
msgid "Start a new game"
msgstr "Start a new game"

#: ../src/gnome-tetravex.vala:170
msgid "Give up and view the solution"
msgstr ""

#: ../src/gnome-tetravex.vala:239
msgid "Size could only be from 2 to 6.\n"
msgstr ""

#: ../src/gnome-tetravex.vala:347
msgid "Are you sure you want to give up and view the solution?"
msgstr ""

#: ../src/gnome-tetravex.vala:349 ../src/gnome-tetravex.vala:410
#, fuzzy
msgid "_Keep Playing"
msgstr "_Continue playing"

#: ../src/gnome-tetravex.vala:350
msgid "_Give Up"
msgstr ""

#: ../src/gnome-tetravex.vala:384
msgid "Position pieces so that the same numbers are touching each other"
msgstr ""

#: ../src/gnome-tetravex.vala:391
msgid "translator-credits"
msgstr ""
"Adam Weinberger\n"
"Tiffany Antopolski"

#: ../src/gnome-tetravex.vala:409
#, fuzzy
msgid "Are you sure you want to start a new game with a different board size?"
msgstr "Do you want to start a new game with this map?"

#: ../src/gnome-tetravex.vala:411
#, fuzzy
msgid "_Start New Game"
msgstr "Start New Game"

#: ../src/puzzle-view.vala:356
msgid "Paused"
msgstr "Paused"

#: ../src/score-dialog.vala:29
msgid "Quit"
msgstr ""

#: ../src/score-dialog.vala:30
msgid "New Game"
msgstr "New Game"

#: ../src/score-dialog.vala:33
msgid "OK"
msgstr ""

#: ../src/score-dialog.vala:45
msgid "Size:"
msgstr "Size:"

#: ../src/score-dialog.vala:70
msgid "Date"
msgstr ""

#: ../src/score-dialog.vala:73
msgid "Time"
msgstr "Time"
